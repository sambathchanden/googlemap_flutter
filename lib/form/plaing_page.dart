import 'package:flutter/material.dart';
class PlainningPage extends StatelessWidget {
  final String orderName,name,phoneNumber,email,weigh,volume,timeFrom,dateFrom,timeTo,dateTo;

  const PlainningPage({Key key, this.orderName, this.name, this.phoneNumber, this.email, this.weigh, this.volume,this.dateFrom,this.timeFrom,
  this.dateTo,this.timeTo }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _buildBody(),
    );
  }

  _buildBody() {
    int year = int.parse(dateFrom.substring(0,4));
    int month = int.parse(dateFrom.substring(5,7));
    int day = int.parse(dateFrom.substring(8,10));
    int hr = int.parse(timeFrom.substring(0,2));
    int mn = int.parse(timeFrom.substring(3,5));

    int yearTo = int.parse(dateTo.substring(0,4));
    int monthTo = int.parse(dateTo.substring(5,7));
    int dayTo = int.parse(dateTo.substring(8,10));
    int hrTO = int.parse(timeTo.substring(0,2));
    int mnTO = int.parse(timeTo.substring(3,5));


    // String str  = DateTime.now(year,month,20).millisecondsSinceEpoch.toString();
    print(DateTime(year, month, day,hr,mn).millisecondsSinceEpoch);
    print(DateTime(yearTo, monthTo, dayTo,hrTO,mnTO).millisecondsSinceEpoch);

    return Container(
      child: Center(
        child: Text(
            "Name: $name \nOrder Name $orderName\nPhone Number: $phoneNumber\nEmail: $email\nWeight: $weigh\nVolume: $volume\nDate From: $year\nTime From: $timeFrom"
                "\n$year\n$month\n$day\n$hr\n$mn"
        ),
      ),
    );
  }
}
