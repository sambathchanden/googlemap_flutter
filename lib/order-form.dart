import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
 import 'package:intl/intl.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:o_logistic/detailpage/detail_page.dart';
import 'package:o_logistic/form/plaing_page.dart';

import 'order_map.dart';
class OrderForm extends StatefulWidget {
  OrderForm({Key key, this.submit}) : super(key: key);
  final String submit; //print

  @override
  _FormSubmitState createState() => _FormSubmitState();
}
class _FormSubmitState extends State<OrderForm> {
  final scaffoldKey = new GlobalKey<ScaffoldState>();
  final _formKey = new GlobalKey<FormState>();
  // bool _autovalidate = true; //validate
  // SecondScreen secondScreen = SecondScreen();
  //Todo:  print error user forget input :(start statement)
  void validate() {
    if (formkey.currentState.validate()) {
      print("validate");
    } else {
      print("Not Validate");
    }
  } //end line
  String validatephone(value) {
    //Todo: build the validate TextField phone:(start statement)
    if (value.isEmpty) {
      return 'Please Input your phone';
    } else if (value.length < 9) {
      return "Should be at least 10 number";
    } else if (value.length > 10) {
      return "Should be more than 10 number";
    }
  } //end statement
  //Todo: constructor build create print data in console run
  final ordername = TextEditingController();
  final name = TextEditingController();
  final phone = TextEditingController();
  final email = TextEditingController();
  final volume = TextEditingController();
  final weight = TextEditingController();
  final dateform = TextEditingController(); // end statement
  getItemAndNavigate(BuildContext context) {
    //Todo build push screen one to another screen
    // Navigator.push(
    //     context,
    //     MaterialPageRoute(
    //         builder: (context) => SecondScreen(
    //           ordernameHolder: this._ordernameController.text,
    //           nameHolder: this._nameController.text,
    //           numberHolder: this._phoneController.text,
    //           emailHolder: this._emailController.text,
    //           volumeHolder: this._VolumeController.text,
    //           weightHolder: this._weightController.text,
    //           dateformHolder: this._dateformController.text,
    //         )));
  } //end statement
  //Todo: constructor build create print data in console run
  var _email;
  var _phone;
  var _ordername;
  var _name;
  var _Volume;
  var _weight;
  var _dateform;
  var _dateto;
  var _timeform;
  var _timeto;
  final TextEditingController _ordernameController =
  new TextEditingController();
  final TextEditingController _emailController = new TextEditingController();
  final TextEditingController _phoneController = new TextEditingController();
  final TextEditingController _nameController = new TextEditingController();
  final TextEditingController _VolumeController = new TextEditingController();
  final TextEditingController _weightController = new TextEditingController();
  final TextEditingController _dateformController = new TextEditingController();
  final TextEditingController _datetoController = new TextEditingController();
  final TextEditingController _timeformController = new TextEditingController();
  final TextEditingController _timetoController = new TextEditingController();
  void _loginButton(
      {String name,
        String phone,
        String email,
        String volume,
        String weight,
        String user,
        String date,
        String dateto,
        String time,
        String timeto}) {
    print("Fill Your Information In Form");
    this._ordername = name;
    this._email = email;
    this._phone = phone;
    this._name = user;
    this._Volume = volume;
    this._weight = weight;
    this._dateform = date;
    this._dateto = dateto;
    this._timeform = time;
    this._timeto = timeto;
    print(_ordername);
    print(_email);
    print(_phone);
    print(_name);
    print(_Volume);
    print(_weight);
    print(_dateform);
    print(_dateto);
    print(_timeform);
    print(_timeto);
  } //end statement of print data in console
  //global key
  final formkey = GlobalKey<FormState>();
  ////Todo: build datetime statement and build alert in TextField time  when select
  var textInput = TextEditingController();
  var textInput2 = TextEditingController();
  var pickedtime;
  final format = DateFormat("yyyy-MM-dd"); //end statement
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        title: Text('Create Order'),
        centerTitle: mounted,
      ),
      body: Container(
        //Space text down
        padding: EdgeInsets.all(12.0),
        child: Form(
          //autovalidate: _autovalidate,
          key: _formKey,
          //the line run scroll on screen
          child: SingleChildScrollView(
            //Create Column TextField
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  'Order name',
                  style: TextStyle(
                    height: 2.0,
                  ),
                ),
                TextFormField(
                  controller: this._ordernameController,
                  keyboardType: TextInputType.text,
                  style: TextStyle(fontSize: 15),
                  decoration: InputDecoration(
                    prefixIcon: Icon(
                      Icons.verified_user,
                      color: Colors.blue,
                    ),
                    labelText: "ORDER NAME",
                    hintText: "Enter your order name",
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(40),
                    ),
                  ),
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Please enter some text';
                    }
                    return null;
                  },
                ),
                Text(
                  'Name',
                  style: TextStyle(
                    height: 2.0,
                  ),
                ),
                TextFormField(
                    controller: this._nameController,
                    decoration: InputDecoration(
                      prefixIcon: Icon(
                        Icons.person,
                        color: Colors.blue,
                      ),
                      labelText: "NAME",
                      hintText: "Type your name",
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(40),
                      ),
                    ),
                    validator: (String value) {
                      if (value.isEmpty) {
                        return 'Please Input Name';
                      }
                      return null;
                    },
                    onSaved: (String value) {}),
                Text(
                  'Phone',
                  style: TextStyle(height: 2.0),
                ),
                TextFormField(
                  controller: this._phoneController,
                  decoration: InputDecoration(
                    //the line add prefixIcon in TextField
                    prefixIcon: Icon(
                      Icons.phone,
                      color: Colors.blue,
                    ),
                    hintText: "Type your phone details",
                    labelText: "PHONE",
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(40),
                    ),
                  ),
                  validator: validatephone,
                ),
                Text(
                  'Email',
                  style: TextStyle(height: 2.0),
                ),
                TextFormField(
                  controller: this._emailController,
                  style: TextStyle(fontSize: 15),
                  decoration: InputDecoration(
                    prefixIcon: Icon(
                      Icons.email,
                      color: Colors.blue,
                    ),
                    hintText: "Type your Email Address",
                    labelText: "EMAIL",
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(40),
                    ),
                  ),
                  validator: MultiValidator([
                    RequiredValidator(errorText: "Required"),
                    EmailValidator(errorText: "Not a vail email"),
                  ]),
                ),
                Text(
                  'Volume',
                  style: TextStyle(height: 2.0),
                ),
                TextFormField(
                    controller: this._VolumeController,
                    decoration: InputDecoration(
                      prefixIcon: Icon(
                        Icons.volume_up,
                        color: Colors.blue,
                      ),
                      hintText: "Type Your Volume",
                      labelText: "VOLUME",
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(40),
                      ),
                    ),
                    validator: (String value) {
                      if (value.isEmpty) {
                        return 'Please Input your volume';
                      }
                      return null;
                    },
                    onSaved: (String value) {}),
                Text(
                  'Weight',
                  style: TextStyle(height: 2.0),
                ),
                TextFormField(
                    controller: this._weightController,
                    decoration: InputDecoration(
                      prefixIcon: Icon(
                        Icons.line_weight,
                        color: Colors.blue,
                      ),
                      hintText: "Type Your Weight",
                      labelText: "WEIGHT",
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(40),
                      ),
                    ),
                    validator: (String value) {
                      if (value.isEmpty) {
                        return 'Please Input your weight';
                      }
                      return null;
                    },
                    onSaved: (String value) {}),
                //create text on TextField row
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 25, vertical: 10),
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: Text('Date Form'),
                      ),
                      Container(
                        child: Expanded(
                          child: Text('Time Form'),
                        ),
                      ),
                    ],
                  ),
                ),


                Container(
                  
                  padding: EdgeInsets.all(0.8),
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: Container(
                          child: DateTimeField(

                            controller: this._dateformController,
                            decoration: InputDecoration(
                              hintText: "Select Date From",
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(40),
                              ),
                            ),
                            format: format,
                            onShowPicker: (context, currentValue) {
                              return showDatePicker(
                                  context: context,
                                  firstDate: DateTime(2000),
                                  initialDate: currentValue ?? DateTime.now(),
                                  lastDate: DateTime(2024),
                                  builder:
                                      (BuildContext context, Widget picker) {
                                    return Theme(
                                      //TODO: change colors date picker
                                      data: ThemeData.dark().copyWith(
                                        colorScheme: ColorScheme.dark(
                                          primary: Colors.deepPurple,
                                          onPrimary: Colors.white,
                                          surface: Colors.green,
                                          onSurface: Colors.white,
                                        ),
                                        dialogBackgroundColor:
                                        Colors.indigoAccent[900],
                                      ),
                                      child: picker,
                                    );
                                  });
                            },
                          ), //end statement
                        ),
                      ),
                      Expanded(
                        child: Container(
                          margin: EdgeInsets.only(left: 10),
                          child: TextFormField(
                            decoration: InputDecoration(
                              prefixIcon: Icon(
                                Icons.timer,
                                color: Colors.blue,
                              ),
                              hintText: "Select Time From",
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(40),
                              ),
                            ),
                            controller:
                            textInput, //this  is got data press time
                            readOnly: true,
                            onTap: () {
                              DatePicker.showTimePicker(context,
                                  showTitleActions: true, onConfirm: (time) {
                                    setState(() {
                                      textInput.text =
                                          DateFormat("hh:mm.a").format(time);
                                    });
                                  },
                                  currentTime: DateTime.now(),
                                  locale: LocaleType.it);
                            },
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 20),
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: Text(
                          'Date To',
                        ),
                      ),
                      Expanded(
                        child: Text('Time To'),
                      ),
                    ],
                  ),
                ),
                Container(
                  padding: EdgeInsets.all(1),
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: Container(
                          margin: EdgeInsets.all(1),
                          child: DateTimeField(
                            controller: this._datetoController,
                            decoration: InputDecoration(
                              hintText: "Select Date To",
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(40),
                              ),
                            ),
                            format: format,
                            onShowPicker: (context, currentValue) {
                              return showDatePicker(
                                  context: context,
                                  firstDate: DateTime(2000),
                                  initialDate: currentValue ?? DateTime.now(),
                                  lastDate: DateTime(2024));
                            },
                          ),
                        ),
                      ),
                      Expanded(
                        child: Container(
                          margin: EdgeInsets.only(
                            left: 10,
                          ),
                          child: Container(
                            child: TextFormField(
                              decoration: InputDecoration(
                                prefixIcon: Icon(
                                  Icons.timer,
                                  color: Colors.blue,
                                ),
                                hintText: "Select Time To",
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(40),
                                ),
                              ),

                              controller:
                              textInput2, //this  is got data press time
                              //readOnly: true,
                              onTap: () {
                                DatePicker.showTimePicker(context,
                                    // showTitleActions: true,
                                    onConfirm: (time) {
                                      setState(() {
                                        textInput2.text =
                                            DateFormat("hh:mm.a").format(time);
                                      });
                                    },
                                    currentTime: DateTime.now(),
                                    locale: LocaleType.it);
                              },
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                //Todo: build RaiseButton submit
                //Todo: The line edit RaiseButton small or large RaiseButton
                Container(
                  padding: EdgeInsets.all(18),
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        //build print data form TextField
                        child: RaisedButton(
                            padding: EdgeInsets.all(20),
                            child: new Text(
                              "Submit",
                              style:
                              TextStyle(color: Colors.white, fontSize: 18),
                            ),
                            color: Colors.blue,
                            shape: RoundedRectangleBorder(
                              //Todo: shape style of RaiseButton
                              borderRadius: BorderRadius.circular(40.0),
                            ),
                            //Todo: constructor build create print data in console run
                            onPressed: () {
                              _loginButton(
                                name: this._ordernameController.text,
                                email: this._emailController.text,
                                phone: this._phoneController.text,
                                user: this._nameController.text,
                                volume: this._VolumeController.text,
                                weight: this._weightController.text,
                                date: this._dateformController.text,
                                dateto: this._datetoController.text,
                                time: this._timeformController.text,
                                timeto: this._timetoController.text,
                              );
                              if (_formKey.currentState.validate()) {
                                //Todo: Build if true or fail use input
                                _formKey.currentState.save();
                                // Todo: Build push data screen one to another screen
                                Navigator.push(
        context,

                                MaterialPageRoute(
            builder: (context) => OrderMap1(
              orderName: this
                  ._ordernameController
                  .text,
              name:
              this._nameController.text,
              phoneNumber:
              this._phoneController.text,
              email:
              this._emailController.text,
              volume:
              this._VolumeController.text,
              weigh:
              this._weightController.text,
              dateFrom:
              this._dateformController.text,
              timeFrom:
              this.textInput.text,
              dateTo: _datetoController.text,
              timeTo: textInput2.text,
            )));
  }
}),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
