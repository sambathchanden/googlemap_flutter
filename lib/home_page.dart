import 'package:flutter/material.dart';

import 'category/category.dart';
class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("O-Logistic",style: TextStyle(color: Colors.white)),
        centerTitle: true,
        actions: <Widget>[
          IconButton(icon: Icon(Icons.add_shopping_cart),
            onPressed: (){
            },
          ),
          IconButton(
            color: Colors.white,
            icon: Icon(Icons.create),
            onPressed: (){
              _createAleartDialog(context);

            },
          )
        ],
      ),
      body: _buildBody(),
    );
  }

  _buildBody() {
    return Container(
      padding: EdgeInsets.all(10),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Expanded(
              child: Category())
      ],
      ),
    );
  }

   _createAleartDialog(BuildContext context) {
    TextEditingController textEditingController = TextEditingController();
    return showDialog(
        context: context,
        builder: (context){

          return Container(
            child: AlertDialog(
              shape: RoundedRectangleBorder(
                  borderRadius:
                  BorderRadius.all(
                      Radius.circular(10.0))),
              content: Builder(
                builder: (context) {
                  return SingleChildScrollView(
                    child: Container(
                      width: 900,
                      child: Column(
                        children: <Widget>[
                          TextField(
                            decoration: InputDecoration(
                              hintText: "please input your name"
                            ),
                          ),
                          TextField(
                            decoration: InputDecoration(
                                hintText: "please input your name"
                            ),
                          ),
                          TextField(
                            decoration: InputDecoration(
                                hintText: "please input your name"
                            ),
                          )
                        ],
                      ),
                    ),
                  );
                },
              ),
            ),
          );
        });
   }
}
