import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:http/http.dart';
import 'package:o_logistic/model/posts.dart';

class DetailPage extends StatelessWidget {
   final Posts posts;
    DetailPage({Key key,this.posts}) : super(key:key);

   @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(posts.title),
      ),
      body: OrderMap(),
    );
  }

}
class OrderMap extends StatefulWidget {
  @override
  _OrderMapState createState() => _OrderMapState();
}

class _OrderMapState extends State<OrderMap> {

  GoogleMapController googleMapController;
  String searchAddress;
  Set<Marker> _markers = {};
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Stack(
        children: [
          GoogleMap(
            onMapCreated: onMapCreated,
            markers: _markers,
            initialCameraPosition: CameraPosition(
              target: LatLng(11.5364018,104.9129842),zoom: 15.0,
            ),
          ),
          Positioned(
            top: 55,
            right: 15.0,
            left: 15.0,
            child: Container(
              height: 50.0,
              width: double.infinity,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10.0),
                  color: Colors.white

              ),
              child: TextField(
                decoration: InputDecoration(
                    hintText: "Please input you address",
                    border: InputBorder.none,
                    contentPadding: EdgeInsets.all(13),
                    suffixIcon: IconButton(
                      icon: Icon(Icons.search),
                      onPressed:searchandNavigate,
                      iconSize: 30,
                    )
                ),
                onChanged: (val){
                  setState(() {
                    searchAddress = val;
                  });
                },
              ),
            ),

          )

        ],
      ),
    ) ;

  }
  searchandNavigate(){
    Geolocator().placemarkFromAddress(searchAddress).then((result){
      googleMapController.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
        target:
        LatLng(result[0].position.latitude, result[0].position.longitude),
        zoom: 15.0,
      )));
      setState(() {
        _markers.add(Marker(
            markerId:MarkerId('<MARKER_ID>'),
            icon: BitmapDescriptor.defaultMarker,
            position:LatLng(result[0].position.latitude, result[0].position.longitude),
            onTap: (){
              print("clicked on marker");
            }
        ));
      });
    });
  }
  onMapCreated(controller) {
    setState(() {
      googleMapController = controller;
      _markers.add(Marker(
          markerId:MarkerId('<MARKER_ID>'),
          icon: BitmapDescriptor.defaultMarker,
          position:LatLng(11.5364018,104.9129842),
          infoWindow: InfoWindow(title:"my marker")
      ));
    });

  }

}

