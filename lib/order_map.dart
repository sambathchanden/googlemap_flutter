
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:http/http.dart';

import 'model/posts.dart';
    class OrderMap1 extends StatefulWidget {
      final String orderName,name,phoneNumber,email,weigh,volume,timeFrom,dateFrom,timeTo,dateTo;
      const OrderMap1({Key key, this.orderName, this.name, this.phoneNumber, this.email, this.weigh, this.volume,this.dateFrom,this.timeFrom,
        this.dateTo,this.timeTo }) : super(key: key);
      @override
      _OrderMapState createState() => _OrderMapState();
    }
    class _OrderMapState extends State<OrderMap1> {

      GoogleMapController googleMapController;
      String searchAddress;
      Set<Marker> _markers = {};

      @override
      Widget build(BuildContext context) {
        return Scaffold(

          body: _buildBody(),
        );
      }

  _buildBody() {

    return Container(
      child: Stack(
        children: [
          GoogleMap(
            onMapCreated: onMapCreated,
            markers: _markers,
            initialCameraPosition: CameraPosition(
              target: LatLng(11.5364018,104.9129842),zoom: 15.0,
            ),
          ),
          Positioned(
            top: 55,
            right: 15.0,
            left: 15.0,
            child: Container(
              height: 50.0,
              width: double.infinity,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10.0),
                color: Colors.white

              ),
              child: TextField(
                decoration: InputDecoration(
                  hintText: "Please input you address",
                  border: InputBorder.none,
                  contentPadding: EdgeInsets.all(13),
                  suffixIcon: IconButton(
                    icon: Icon(Icons.search),
                    onPressed:searchandNavigate,
                    iconSize: 30,
                  )
                ),
                onChanged: (val){
                  setState(() {
                   
                    searchAddress = val;
                  });
                },
              ),
            ),

          )
          
        ],
      ),
    ) ;
  }
    searchandNavigate(){
        if(searchAddress == null){
          print("error");
        } else{
          Geolocator().placemarkFromAddress(searchAddress).then((result){
            googleMapController.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
              target:
              LatLng(result[0].position.latitude, result[0].position.longitude),
              zoom: 15.0,
            )));
            setState(() {
              _markers.clear();
              _markers.add(Marker(
                  markerId:MarkerId('<MARKER_ID>'),
                  icon: BitmapDescriptor.defaultMarker,

                  onTap: (){
                    print("clicked on marker");

                  }
              ));

            });
          });
        }
   

    }
   onMapCreated(controller) {
     List<Placemark> placemark;
    setState(() {
      googleMapController = controller;
      _markers.add(Marker(
        markerId:MarkerId('<MARKER_ID>'),
        icon: BitmapDescriptor.defaultMarker,
        position:LatLng(11.5364018,104.9129842),
        infoWindow: InfoWindow(title:"my marker")
      )
      );

    });

  }

       getAddress(double latitude,double longitude) async{
        List<Placemark> placemark;
        String _address;


        placemark = await Geolocator().placemarkFromCoordinates(latitude, longitude);
        _address = placemark[0].name.toString()+
            ","+ placemark[0].locality.toString()+(",Postal Code: "+ placemark[0].postalCode.toString());
        return _address;
      }


    }
