
import 'dart:async';
import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:http/http.dart' as http;
import 'package:o_logistic/detailpage/detail_page.dart';
import 'package:o_logistic/model/posts.dart';
import 'package:o_logistic/order_map.dart';
class TrackingPage extends StatefulWidget {

  @override
  _TrackingPageState createState() => _TrackingPageState();
}
Future<List<Posts>> _getData() async{
  http.Response response = await http.get("https://jsonplaceholder.typicode.com/posts");
  if(response.statusCode == 200){
    return compute(_convert, response.body);
  }else{
    print("error");
  }

}
List<Posts>_convert(String body) {
  List list = json.decode(body);
  List<Posts> mylist = list.map((e)=> Posts.fromMap(e)).toList();
  return mylist;
}
class _TrackingPageState extends State<TrackingPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _buildBody(),
    );
  }

  _buildBody() {
    return Container(
      child:FutureBuilder(
        future: _getData(),
        builder: (context,snapshot){
          if(snapshot.hasError){
            return Center(child: Text("error while loading data"));
          }else{
            if(snapshot.connectionState == ConnectionState.done){
              return _buildListview(snapshot.data);
            }else{
              return SpinKitCircle(color: Colors.blue);
            }
          }
        },
      )
    );
  }

  _buildListview(List<Posts> data) {

    return ListView.builder(
        itemCount: data.length,
        itemBuilder: (context,index){
      return Card(
          child: ListTile(
          leading: Icon(Icons.account_balance),
            title: Text(data[index].title),
            subtitle: Text("10/10/2020"),
            onTap: () => Navigator.push(
                context, MaterialPageRoute(
              builder: (context) => DetailPage(
                posts:data[index],
              )
            )),

      )
      );
    });
  }
}


