import 'package:flutter/material.dart';
import 'package:o_logistic/category/o_history.dart';
import 'package:o_logistic/category/o_tracking.dart';

import 'home_page.dart';

class Category extends StatefulWidget {
  @override
  _CategoryState createState() => _CategoryState();
}

class _CategoryState extends State<Category> {
  int selectItem = 0;
  List<String>catogoryList = ["Home","Tracking","History"];
  List<Widget>_pageList;
  HistoryOrder _historyOrder = HistoryOrder();
  TrackingPage _trackingPage = TrackingPage();
  HompageCategory _homePageCategory = HompageCategory();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _pageList = [_homePageCategory,_trackingPage,_historyOrder, ];
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _buildBody(),
    );
  }
  PageController _pageController = PageController();
  _buildBody() {

      return Column(
        children: <Widget>[
          Container(
            height: 50,
            alignment: Alignment.center,
            width: MediaQuery.of(context).size.width,
            // color: Colors.amber,
            child: ListView.builder(
                scrollDirection: Axis.horizontal,
                itemCount: catogoryList.length,
                itemBuilder: (context,index)
                => _buildCategory(index)),

    ),
          Expanded(
            child: Container(
              child: PageView(
                controller: _pageController,
                physics: NeverScrollableScrollPhysics(),
                children:_pageList,
                onPageChanged: (index){
                  setState(() {
                    selectItem = index;
                  });
                },
              ),
            ),
          )
        ],
      );

  }

  _buildCategory(index) {
    return GestureDetector(
      onTap: (){
        setState(() {
          selectItem = index;
          if(selectItem == index){
            _pageController.animateToPage(index, duration: Duration(milliseconds: 50), curve: Curves.easeOut);
          }
        });
      },
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 35),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(catogoryList[index],
            style: TextStyle(fontSize: 17,fontWeight: FontWeight.bold,
            color: selectItem == index ? Colors.blueAccent : Colors.grey)
              ,),
            Container(
              margin: EdgeInsets.all(2),
              height: 2,
              width: 30,
              color: selectItem == index ? Colors.blueAccent : Colors.transparent,
            )
          ],
        ),
      ),
    );
  }
}
