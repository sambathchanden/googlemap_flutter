class Posts{
  int userId,id;
  String title, body;

  Posts({this.userId, this.id, this.title, this.body});

  Posts.fromMap(Map<String, dynamic>map){
    userId = map["userId"];
    id = map["id"];
    title = map["title"];
    body = map["body"];

  }

}